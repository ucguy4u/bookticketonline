CREATE TABLE movie (
    id int  NOT NULL,
    title varchar(256)  NOT NULL,
    director varchar(256)  NULL,
    cast varchar(1024)  NULL,
    description text  NULL,
    duration_min int  NULL
);
ALTER TABLE movie ADD CONSTRAINT movie_PK PRIMARY KEY (ID);




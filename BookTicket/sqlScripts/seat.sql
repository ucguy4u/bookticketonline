CREATE TABLE seat (
    id int  NOT NULL,
    line int  NOT NULL,
    number int  NOT NULL,
    auditorium_id int  NOT NULL
);
ALTER TABLE seat ADD CONSTRAINT seat_pk PRIMARY KEY (ID);

ALTER TABLE reservation ADD CONSTRAINT Reservation_Projection FOREIGN KEY (screening_id) REFERENCES screening (id) ;
ALTER TABLE screening ADD CONSTRAINT Projection_Movie FOREIGN KEY (movie_id) REFERENCES movie (id) ;
ALTER TABLE screening ADD CONSTRAINT Projection_Room FOREIGN KEY (auditorium_id) REFERENCES auditorium (id) ;
CREATE TABLE employee (
    id int  NOT NULL,
    username varchar(32)  NOT NULL,
    password varchar(100)  NOT NULL
);
ALTER TABLE employee ADD CONSTRAINT employee_PK PRIMARY KEY (ID);

CREATE TABLE seat_reserved (
    id int  NOT NULL,
    seat_id int  NOT NULL,
    reservation_id int  NOT NULL,
    screening_id int  NOT NULL
);
ALTER TABLE seat_reserved ADD CONSTRAINT seat_reserved_pk PRIMARY KEY (ID);

CREATE TABLE auditorium (
    id int  NOT NULL,
    name varchar(32)  NOT NULL,
    seats_no int  NOT NULL
);
ALTER TABLE auditorium ADD CONSTRAINT auditorium_PK PRIMARY KEY (ID);
CREATE TABLE reservation (
    id int  NOT NULL,
    screening_id int  NOT NULL,
    employee_reserved_id int  NULL,
    reservation_type_id int  NULL,
    reservation_contact varchar(1024)  NOT NULL,
    reserved bool  NULL,
    employee_paid_id int  NULL,
    paid bool  NULL,
    active bool  NOT NULL);
ALTER TABLE reservation ADD CONSTRAINT reservation_pk PRIMARY KEY (ID);

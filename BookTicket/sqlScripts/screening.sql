CREATE TABLE screening (
    id int  NOT NULL,
    movie_id int  NOT NULL,
    auditorium_id int  NOT NULL,
    screening_start timestamp  NOT NULL
);

ALTER TABLE screening ADD CONSTRAINT Projection_ak_1 UNIQUE (auditorium_id, screening_start);

ALTER TABLE screening ADD CONSTRAINT screening_pk PRIMARY KEY (ID);

